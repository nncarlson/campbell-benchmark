# Computational Domain

![geometry](geom.png)

# Meshes

* The "cast" meshes consist only of the mold cavity that is to be filled
  with liquid aluminum
* The "full" meshes include also the sand mold.
* The casting volume is hex meshed with a just a few wedge elements at
  the end of the runner.
* The mold volume is tet meshed with a layer of pyramid elements bridging
  to the hex meshed casting volume.
* The indicated resolution is the minimum linear resolution in the main
  part of the casting volume. The resolution in the narrow-most part of
  the sprue is approximately half that.
* The "p" ("plus") meshes have a finer resolution through the thickness
  of the plate (the z-direction) and in the x-direction of the sprue that
  matches the resolution of the next finer mesh.
* The "0p" casting volume mesh is basically the same as used in the Flow-3D
  paper (aside from conforming to the non-orthogonal geometry instead of
  being stair-stepped).

Mesh | cast | full | resolution
:--- | ---: | ---: | ---:
0    | 2.7K |  66K |   5mm
0p   | 6.4K | 109K | 2.5mm
1    |  21K | 364K | 2.5mm
1p   |  35K | 470K | 1.67mm
2    |  73K | 1.1M | 1.67mm
2p   | 102K | 1.3M | 1.25mm
3    | 171K | 2.4M | 1.25mm

# Element Blocks

block | description
:-: | :-
1 | casting volume
2 | mold volume

# Side Sets

side set | description
:-: | :-
1 | top of the sprue / fluid inlet boundary
2 | external mold volume boundary except for its top-most side
3 | interface between the casting and mold volumes (2-sided)
4 | top-most side of the mold volume

* Note: the y-axis is the vertical axis
